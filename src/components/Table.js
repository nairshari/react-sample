import React, { Component } from 'react';
import Search from './search';
import Select from './Select';
import Checkbox from './checkbox';
const list = [
  {
    name: 'Jonny',
    age: '25',
    city: 'abc',
    zip_code: 38965,
    country: "India",
    oid: 1,
  },
  {
    name: 'Anmol',
    age: '15',
    city: 'abc',
    zip_code: 38965,
    country: "Libya",
    oid: 2,
  },
  {
    name: 'Ankit',
    age: '12',
    city: 'abc',
    zip_code: 32368,
    country: "Nepal",
    oid: 3,
  },
  {
    name: 'Manis',
    age: '45',
    city: 'abc',
    zip_code: 32365,
    country: "India",
    oid: 4,
  },
  {
    name: 'Daksh',
    age: '29',
    city: 'abc',
    zip_code: 38963,
    country: "Japan",
    oid: 5,
  },
];
function isSearched(searchTerm,selectTerm, checked) {
  return function (item) {
    if(checked == true){
      return item.age < 18;
    }
   else if((searchTerm != '')){
      return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;
   }
   else{
    return item.country.toLowerCase().indexOf(selectTerm.toLowerCase()) !== -1;
   }
 }
}
class Table extends Component {
  constructor(props){
    super(props);
    this.state = {
     list: list,
     searchTerm: '',
     selectTerm: '',
     checked: false,
    };
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onDismiss = this.onDismiss.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

  }
  onDismiss(id) {
   const isNotId = item => item.oid !== id;
   const updatedList = this.state.list.filter(isNotId);
   this.setState({ list: updatedList });
  }

  onSearchChange(event) {
   this.setState({ searchTerm: event.target.value });
  }

  handleChange(event){
    this.setState({selectTerm: event.target.value});
  }

  handleInputChange(event){
    this.setState({checked: event.target.checked});
  }

  render() {
    const { searchTerm, list,selectTerm, checked } = this.state;
    return (
      <>
      <h2 className = "heading">Demo React App</h2>
     <div className ="list-container" >
      <div className = "search-box">
         <Search
            value={searchTerm}
            onChange={this.onSearchChange}
            >
            Type for search....
         </Search>
      </div>
      <div className = "select-box">
         <Select
           value={selectTerm}
            onChange={this.handleChange}
          >
         </Select>
      </div>
      <div className = "checkbox">
         <Checkbox
          value = {checked}
          onChange={this.handleInputChange}
         />
      </div>
      <div className = "user-list main-container">
        {list.filter(isSearched(searchTerm,selectTerm, checked)).map(item =>
        <div key={item.oid} className= "container">
        <table>
          <tbody>
            <tr>
              <td className = "name">{item.name}</td>
              <td className = "age">{item.age}</td>
              <td className = "zip-code">{item.zip_code}</td>
              <td className = "city">{item.city}</td>
              <td className = "country">{item.country}</td>
              <td className = "test">
                <button className = "delete" 
                  onClick={() => this.onDismiss(item.oid)}
                  type="button"
                  >
                  Dismiss
                </button>
              </td>
            </tr>
          </tbody>
        </table>
        </div>
        )}
      </div>
      </div>
      </>
    );
  }
}
export default Table;
