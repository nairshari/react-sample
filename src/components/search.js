import React, { Component } from 'react';

class Search extends Component { 
  render() {
    const {value,onChange,children} = this.props;
    return (
      <>
      <form>
        <input type="text" 
        name="name"
        value={value}
        onChange={onChange}
        placeholder = "Type name to search"/>
      </form>
      </>
    );
  }
}
export default Search;
