import React, { Component } from 'react';
import Toggle from 'react-toggle'

function Checkbox(props){
   const { value, onChange} = props;
    return(
      <label>
        <Toggle
          defaultChecked={value}
          icons={false}
          onChange={onChange} />
           <h4 className= "age-text">Age below 18 </h4>
       </label>
      );
    }

export default Checkbox;




