import React, { Component } from 'react';

class Select extends Component {
  render(){
     const { value, onChange} = this.props;
    return(
      <label>
          <select defaultValue={value}
            onChange={onChange}>
            <option value=''>Select Country</option>
            <option value="INDIA">INDIA</option>
            <option value="LIBYA">Libya</option>
            <option value="NEPAL">Nepal</option>
            <option value="JAPAN">Japan</option>
          </select>
          {value== '' ? 
            <div>
              <h4>Selected value:Nothing Selected </h4>
            </div>
           : (
            <div>
              <h4>Selected value:{value}</h4>
            </div>
          )
        }
      
        </label>
      );
  }
}
export default Select;



